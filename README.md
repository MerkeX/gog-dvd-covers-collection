## GOG Covers

A list of covers for the GOG versions of videogames.

#### Disclaimer

These covers are derivative from other people on deviantart:
* Arcangel33 (https://www.deviantart.com/arcangel33);
* XOVYANT (https://www.deviantart.com/xovyant)

#### List of completed Covers

- 198X
- A Plague Tale - Innocence
- A Story About My Uncle
- Alan Wake
- Alan Wake's American Nightmare
- Alien Isolation
- Amid Evil
- Amnesia: The Dark Descent
- Amnesia: Machine for Pigs
- Amnesia: Rebirth
- Ashen
- Assassin's Creed - Director's Cut
- Batman Arkham Asylum
- Batman Arkham City
- Batman Arkham Origins
- Batman Arkham Knight
- Beneath A Steel Sky
- Beyond A Steel Sky
- Bioshock Remastered
- Bioshock 2 Remastered
- Bioshock Infinite
- Blade Runner
- Blair Witch
- Bloodstained - Ritual Of The Night
- Braid
- Call of Chtulu
- Chernobylite
- Close to the Sun
- Conglomerate 451
- Control - Ultimate Edition
- Creaks
- Cuphead
- Cyberpunk 2077
- Dead Space
- Destroy All Humans
- Deus EX - Goty Edition
- Deus Ex - Human Revolution
- Deus Ex - Mankind Divided
- Dishonored - Complete Edition
- Distrust
- Doom Collection
- Doom II
- Dungeons 2
- Dying Light: The Following - Enhanced Edition
- Fallout
- Fallout 2
- Fade to Silence
- Ghostrunner
- Gone Home
- Grim Fandango Remastered
- Hellpoint
- Horizon Zero Dawn - Complete Edition
- Hotline Miami
- Hotline Miami 2: Wrong Number
- Just Cause
- Just Cause 2
- Killer is Dead
- Kingdom Come Deliverance
- Kingdom Come Deliverance - Royal Edition
- Layers of Fear
- Limbo
- Life Is Strange
- Life Is Strange - Before the Storm
- Little Nightmares II
- Mafia - Definitive Edition
- Mafia 2 - Definitive Edition
- Mafia 3 - Definitive Edition
- Mafia Trilogy
- Metal Gear
- Metal Gear 2
- Metal Gear Solid
- Metal Gear Solid 2
- Metro 2033 Redux
- Metro Last Light Redux
- Metro Exodus
- Middle Earth: Shadow Of Mordor GOTY
- Middle Earth: Shadow Of War
- Mind Scanners
- Mirror's Edge
- Mortal Shell
- Never Alone
- Nex Machina
- Observation
- _>Observer - System Redux
- Oxenfree
- Panzer Dragoon Remake
- Phantom Doctrine
- Prey (2017)
- Quake I - The Offering
- Quake II - Quad Damage
- Quake III - Gold
- Quake Remastered
- QUBE: Director's Cut
- Return to Castle Wolfenstein
- Shadowrun Dragonfall
- Shadowrun Hong Kong
- Shadowrun Returns
- Shadowrun Returns (alternative version)
- Song of Horror - Complete Edition
- Spec Ops - The Line
- STALKER - Complete Series
- Star Renegades
- Steel Rats
- Syberia
- Syberia 2
- Syberia 3
- Syberia: Complete Collection
- System Shock: Enhanced Edition
- System Shock 2
- Tales from the Borderlands
- The Elder Scrolls I: Arena
- The Elder Scrolls II: Daggerfall
- The Elder Scrolls III: Morrowind GOTY Edition
- The Elder Scrolls IV: Oblivion
- The Elder Scrolls V: Skyrim - Anniversary Edition
- The Evil Within
- The Evil Within 2
- The First Templar - Special Edition
- The Medium
- The Outer Worlds
- The Talos Principle - Gold Edition
- Tomb Raider (2013) - GOTY Edition
- Transient
- Trine Enhanced Edition
- Trine 2: Complete Story
- Ultimate Doom
- Virtua Verse
- Visage
- Walking Dead S4
- Wasteland Remastered
- Wasteland 2 Director's Cut - Deluxe Edition
- Wasteland 3 - Deluxe Edition
- Wolfenstein: The New Order
- Wolfenstein: The Old Blood

#### List of Retail (non-GOG) Covers

- Dead Space 2
- Dead Space 3
- Doom (2016)
- Grand Theft Auto: San Andreas
- Left 4 Dead
