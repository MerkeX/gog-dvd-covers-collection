GOG Template:
=============

So, Example.xcf is what I would load and start work from there. I have 
the BlankGOG.xcf as a backup/template for the legal stuff and ESRB ratings
stickers. The BlankDisk.xcf has a black or white logo, as well as the ESRB
stickers contained for easy switching. The font I use for the legal and
requirements is Bahnschrift, which should be included in Windows 10.

This template is designed based on Sony's specs for DVD's. The outer
border is technically bleed space, but most people size to fit when they
print anyway, so I just use them as logo and text guides.

-Mobeeuz