## GOG Covers

A list of covers for the GOG versions of videogames.

#### Disclaimer

These covers are derivative from other people on deviantart:
* Arcangel33 (https://www.deviantart.com/arcangel33);
* XOVYANT (https://www.deviantart.com/xovyant)

#### List of completed Covers

- A Plague Tale - Innocence
- Amnesia: Rebirth
- Batman Arkham Asylum
- Batman Arkham City
- Batman Arkham Origins
- Batman Arkham Knight
- Blade Runner
- Call of Chtulu
- Chernobylite
- Close to the Sun
- Control - Ultimate Edition
- Cyberpunk 2077
- Destroy All Humans
- Deus Ex - Human Revolution
- Deus Ex - Mankind Divided
- Dishonored - Complete Edition
- Dying Light: The Following - Enhanced Edition
- Ghostrunner
- Horizon Zero Dawn - Complete Edition
- Kingdom Come Deliverance - Royal Edition
- Life Is Strange
- Life Is Strange - Before the Storm
- Mafia - Definitive Edition
- Mafia 2 - Definitive Edition
- Mafia 3 - Definitive Edition
- Mafia Trilogy
- Metro Exodus
- Mirror's Edge
- Never Alone
- Nex Machina
- _>Observer - System Redux
- Prey (2017)
- Song of Horror - Complete Edition
- Spec Ops - The Line
- The Elder Scrolls IV: Oblivion
- The Evil Within
- The Evil Within 2
- The Outer Worlds
- The Talos Principle - Gold Edition
- Wasteland Remastered
- Wasteland 2 Director's Cut - Deluxe Edition
- Wasteland 3 - Deluxe Edition
