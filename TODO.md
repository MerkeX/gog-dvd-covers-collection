## GOG Covers

A list of covers for the GOG versions of videogames.

#### List of TODO covers

- <s>A Story About My Uncle</s>
- AER Memories of Old
- <s>Alan Wake</s>
- <s>Alan Wake's American Nightmare</s>
- <s>Amnesia: The Dark Descent</s>
- <s>Amnesia: A Machine For Pigs</s>
- <s>Assassin's Creed: Director's Cut</s>
- <s>Bioshock Remastered</s>
- <s>Bioshock 2 Remastered</s>
- <s>Bioshock Infinite Complete Edition</s>
- <s>Braid</s>
- Broforce
- Call of Juarez: Gunslinger
- Crysis
- <s>Cuphead</s>
- <s>Dead Space</s>
- <s>Deus Ex: GOTY Edition</s>
- Dex
- DOOM 3: BFG Edition
- Dragon Age: Origins - Ultimate Edition
- <s>Dungeons 2</s>
- F.E.A.R. Platinum
- F.E.A.R. 2: Project Origins
- <s>Fallout</s>
- <s>Fallout 2</s>
- Fallout 3: GOTY Edition
- Fallout: New Vegas Ultimate Edition
- Far Cry
- Far Cry 2: Fortune's Edition
- GreedFall
- GRIS
- Hard Reset Redux
- Hellblade: Senua's Sacrifice
- Hollow Knight
- <s>Hotline Miami</s>
- <s>Hotline Miami 2: Wrong Number</s>
- HYPERGUN
- <s>Layers of Fear</s>
- Layers of Fear 2
- Lords of the Fallen GOTY Edition
- LOST EMBER
- <s>MDK</s>
- <s>Metro 2033 Redux</s>
- <s>Metro Last Light Redux</s>
- My Friend Pedro
- No Man's Sky
- Obduction
- Outlast
- Outlast 2
- Overlord
- Overlord II
- <s>Oxenfree</s>
- PC Building Simulator
- Pine
- Prison Architect
- <s>Q.U.B.E. Director's Cut</s>
- Q.U.B.E. 2
- <s>Quake: The Offering</s>
- <s>Quake II: Quad Damage</s>
- <s>Quake III: Arena</s>
- Quake 4
- Redout: Solar Challenge Edition
- Relicta
- RiME
- S.T.A.L.K.E.R.: Shadow of the Chernobyl
- S.T.A.L.K.E.R.: Clear Sky
- S.T.A.L.K.E.R.: Call of Pripyat
- S.T.A.L.K.E.R. 2
- Saints Row 2
- Saints Row: The Third - The Full Package
- Saints Row IV: Game of the Century Edition
- Shadow Tactics: Blades of the Shogun
- Shadow Warrior
- Shadow Warrior 2
- SHENZEN I/O
- Sniper Elite V2 Remastered
- Sniper Ghost Warrior
- Sniper Ghost Warrior 2
- Sniper Ghost Warrior 3
- Star Wars Battlefront (Classic, 2004)
- <s>Star Wars Battlefront 2 (Classic, 2005)</s>
- Star Wars Republic Commando
- State of Mind
- SuperHot
- SuperHot: Mind Control Delete
- <s>Syberia</s>
- <s>Syberia 2</s>
- <s>Syberia 3: The Complete Journey</s>
- <s>System Shock: Enhanced Edition</s>
- <s>System Shock 2</s>
- System Shock (reboot)
- </s>The Elder Scrolls III: Morrowind GOTY Edition</s>
- <s>The Medium</s>
- The Solus Project
- The Witcher - Enhanced Edition
- The Witcher 2: Assassin of Kings - Enhanced Edition
- The Witcher 3: Wild Hunt - GOTY Edition
- This War of Mine
- <s>Trine Enchanted Edition</s>
- <s>Trine 2: Complete Story</s>
- Trine 3: The Artifacts of Power
- Trine 4: The Nightmare Prince
- Vampyr
- We Happy Few
- West of Dead
- <s>Wolfenstein: The New Order</s>
- <s>Wolfenstein: The Old Blood</s>
- Wolfenstein II: The New Colossus
